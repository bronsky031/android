package com.example.bhagya.tempconvert;

import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;
    Button convertButton;
    RadioGroup rdGrp;
    RadioButton fahrenheit, celcius;
    EditText tempIn;
    TextView tempOut;
    TextView labelFor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        convertButton = (Button) findViewById(R.id.convertbtn);
        rdGrp = (RadioGroup) findViewById(R.id.radioGroup);
        tempIn = (EditText) findViewById(R.id.tempInput);
        tempOut = (TextView) findViewById(R.id.result);
        labelFor = (TextView) findViewById(R.id.resultFor);
        setSupportActionBar(toolbar);

        convertButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tempValue = tempIn.getText().toString();
                String convertTo = ((RadioButton) findViewById(rdGrp.getCheckedRadioButtonId())).getText().toString();

                Double computedValue;
                String outputText;
                if(tempValue.equals("")){
                    tempIn.setError("Please enter a number");
                }
                else {
                    try {
                        Double myValue = Double.parseDouble(tempValue);
                        if (convertTo.equals("Fahrenheit to Celcius")) {
                            computedValue = (myValue - 32)*5/9;
                            outputText = "Temperature in Celcius";
                            tempOut.setText(computedValue.toString());
                            labelFor.setText(outputText);
                        } else if (convertTo.equals("Celcius to Fahrenheit")) {
                            computedValue = (myValue * 9/5)+32;
                            outputText = "Temperature in Fahrenheit";
                            tempOut.setText(computedValue.toString());
                            labelFor.setText(outputText);
                        }
                    } catch (NumberFormatException e){
                        tempIn.setError("Only numbers allowed");
                    }

                }

            }
        });
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.bhagya.tempconvert/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Main Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://com.example.bhagya.tempconvert/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }
}
